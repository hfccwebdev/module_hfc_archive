<?php

/**
 * @file
 * Contains page callbacks.
 */

/**
 * Page callback for the archives.
 */
function hfc_archive_page() {

  $args = func_get_args();
  $target = implode('/', $args);

  $uri = 'archive://' . $target;
  $wrapper = new ArchiveStreamWrapper();
  $wrapper->SetUri($uri);

  $realpath = $wrapper->realpath();
  if (is_file($realpath)) {
    file_download('archive', $target);
  }
  elseif (is_dir($realpath)) {
    $dir = opendir($realpath);
    while (($entry = readdir($dir)) !== false) {
      if (!preg_match('/^\./', $entry))
      $entries[] = $entry;
    }
    closedir($dir);
    sort($entries, SORT_STRING);

    $dirs = array();
    $files = array();
    foreach ($entries as $entry) {
      $target_entry = implode('/', $args + array('entry' => $entry));

      if (is_dir($realpath . '/' . $entry)) {
        $dirs[$target_entry] = l($entry, 'archive/' . $target_entry);
      }
      elseif(is_file($realpath . '/' . $entry)) {
        $filemime = file_get_mimetype($target_entry);
        $target_uri = 'archive/' . $target_entry;
        $files[$target_entry] = l($entry, $target_uri);
      }
    }
    if (!empty($dirs) || !empty($files)) {
      $title = implode('  / ', array('base' => t('HFC Archive')) + $args);
      drupal_set_title($title);
      $output = array();

      if (!empty($dirs)) {
        $output[] = array(
          '#theme' => 'item_list',
          '#items' => $dirs,
          '#attributes' => array('class' => 'archive-directories'),
        );
      }
      if (!empty($files)) {
        $output[] = array(
          '#theme' => 'item_list',
          '#items' => $files,
          '#attributes' => array('class' => 'archive-items'),
        );
      }
      return $output;
    }
  }
}
