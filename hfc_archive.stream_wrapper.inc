<?php

/**
 * @file
 * HFC Archive Stream Wrapper
 */

/**
 * Default files (archive://) stream wrapper class.
 */
class ArchiveStreamWrapper extends DrupalPublicStreamWrapper {
  public function getDirectoryPath() {
    return variable_get('hfc_archive_file_path', '');
  }

  /**
   * Overrides getExternalUrl().
   */
  public function getExternalUrl() {
    $path = str_replace('\\', '/', $this->getTarget());
    return url('archive/' . $path, array('absolute' => TRUE));
  }

}
